module.exports = [
    {
      id: 1,
      title: 'This is a default event 1',
      detail: 'Berlin 1',
      date: '2019-10-21'
    },
    {
      id: 2,
      title: 'This is a default event 2',
      detail: 'Berlin 2',
      date: '2019-11-21'
    },
    {
      id: 3,
      title: 'This is a default event 3',
      date: '2019-11-13'
    }
  ];