FROM node:6.11.5

WORKDIR /usr/src/app
COPY package.json .
RUN npm install
RUN npm install express
RUN npm install method-override
RUN npm install errorhandler
RUN npm install morgan
RUN npm install ejs
COPY . .

CMD [ "npm", "start" ]
